package com.nvanmeurs.weatherat;

/**
 * @author Nicky van Meurs <nvmeurs@outlook.com>
 */
public class Contact {
    protected int id;
    protected String cityName;
    protected String nickName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
