package com.nvanmeurs.weatherat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "weatherat.db";
    private static final String TABLE_NAME = "contacts";
    private static final String KEY_ID = "id";
    private static final String CITYNAME = "cityname";
    private static final String NICKNAME = "nickname";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_NAME = "CREATE TABLE " + TABLE_NAME + "(" +
                KEY_ID + " INTEGER PRIMARY KEY," + CITYNAME + " TEXT," + NICKNAME +
                " TEXT" + ")";
        db.execSQL(CREATE_TABLE_NAME);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void createContact(Contact record)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CITYNAME, record.getCityName());
        values.put(NICKNAME, record.getNickName());

        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public String getContactByNickName(String nickName) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT " + CITYNAME + " FROM " + TABLE_NAME + " WHERE " + NICKNAME + "='" + nickName + "';", null);

        if (cursor.moveToFirst()) {
            return cursor.getString(0);
        }

        return "";
    }

    public void truncate() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public List<Contact> getAllContacts() {
        List<Contact> contactList = new ArrayList<Contact>();
        String query = "SELECT * FROM "+TABLE_NAME+";";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                Contact contact = new Contact();
                contact.setId(Integer.parseInt(cursor.getString(0)));
                contact.setCityName(cursor.getString(1));
                contact.setNickName(cursor.getString(2));
                contactList.add(contact);
            } while(cursor.moveToNext());
        }

        return contactList;
    }
}
