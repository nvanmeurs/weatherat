package com.nvanmeurs.weatherat;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.*;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.Toast;
import java.util.List;

/**
 * @author Nicky van Meurs <nvmeurs@outlook.com>
 */
public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DatabaseHandler db = new DatabaseHandler(this);
        List<Contact> contacts = db.getAllContacts();

        for (Contact contact : contacts) {
            createContactRow(contact.getNickName());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    public void deleteContactsButtonListener(View view) {
        DatabaseHandler db = new DatabaseHandler(this);
        db.truncate();
    }

    public void createContactButtonListener(View view) {
        EditText nickNameField = (EditText) view.findViewById(R.id.nickName);
        EditText cityNameField = (EditText) view.findViewById(R.id.cityName);
        String nickName = nickNameField.getText().toString();
        String cityName = cityNameField.getText().toString();

        Contact contact = new Contact();
        contact.setNickName(nickName);
        contact.setCityName(cityName);

        DatabaseHandler db = new DatabaseHandler(this);
        db.createContact(contact);

        createContactRow(nickName);
    }

    public void createContactRow(String nickName) {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View newRow = layoutInflater.inflate(R.layout.scrollview_tablerow_layout, null);

        Button tagButton = (Button) newRow.findViewById(R.id.tagButton);
        tagButton.setText(nickName);

        TableLayout tagsTableLayout = (TableLayout) findViewById(R.id.queryTableLayout);
        tagsTableLayout.addView(newRow);
    }

    public void editButtonListener(View view) {
        Toast.makeText(
                getApplicationContext(),
                "Edit contact button",
                Toast.LENGTH_LONG
        ).show();
    }

    public void tagButtonListener(View view) {
        Toast.makeText(
                getApplicationContext(),
                "Contact tag button",
                Toast.LENGTH_LONG
        ).show();
    }
}
